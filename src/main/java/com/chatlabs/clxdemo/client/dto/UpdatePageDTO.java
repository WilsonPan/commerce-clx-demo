package com.chatlabs.clxdemo.client.dto;

import java.sql.Date;

import com.alibaba.fastjson.JSONObject;

import lombok.Data;

@Data
public class UpdatePageDTO {
    private String id;
    private String title;
    private String remark;
    private JSONObject data;
    private Date updateTime;
}
