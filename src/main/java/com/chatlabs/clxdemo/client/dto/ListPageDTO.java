package com.chatlabs.clxdemo.client.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.chatlabs.clxdemo.domain.Page.PageStatus;

import lombok.Data;

@Data
public class ListPageDTO {
    private String id;
    private String title;
    private PageStatus status;
    private String remark;
    private String channel;
    private String opertor;
    @JSONField(name = "create_time", format = "yyyy-MM-dd HH:mm:ss")
    private String createTime;

}
