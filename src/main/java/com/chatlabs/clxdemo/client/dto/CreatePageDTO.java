package com.chatlabs.clxdemo.client.dto;

import com.alibaba.fastjson.JSONObject;

import lombok.Data;

@Data
public class CreatePageDTO {
    private String title;
    private String remark;
    private JSONObject data;
}