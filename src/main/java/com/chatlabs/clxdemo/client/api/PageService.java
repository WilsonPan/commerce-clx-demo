package com.chatlabs.clxdemo.client.api;

import java.util.List;

import com.chatlabs.clxdemo.client.dto.CreatePageDTO;
import com.chatlabs.clxdemo.client.dto.ListPageDTO;

import org.bson.types.ObjectId;

public interface PageService {
    ObjectId save(CreatePageDTO createPageDTO);

    List<ListPageDTO> list();
}
