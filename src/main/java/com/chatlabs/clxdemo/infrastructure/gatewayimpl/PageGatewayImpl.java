package com.chatlabs.clxdemo.infrastructure.gatewayimpl;

import com.chatlabs.clxdemo.domain.Page.Page;
import com.chatlabs.clxdemo.domain.gateway.PageGateway;
import com.chatlabs.clxdemo.infrastructure.convertor.PageConvertor;
import com.chatlabs.clxdemo.infrastructure.dataobject.PageDO;
import com.chatlabs.clxdemo.infrastructure.repository.PageRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PageGatewayImpl implements PageGateway {

    @Autowired
    private PageRepository pageRepository;

    @Override
    public ObjectId savePage(Page page) {

        PageDO pageDO = this.pageRepository.save(PageConvertor.INSTANCE.toPageDO(page));

        return pageDO.getId();
    }

}
