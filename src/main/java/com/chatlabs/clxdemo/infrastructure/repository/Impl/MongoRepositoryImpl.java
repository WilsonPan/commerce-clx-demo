package com.chatlabs.clxdemo.infrastructure.repository.Impl;

import com.chatlabs.clxdemo.infrastructure.repository.MongoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import lombok.Getter;

@Repository
public class MongoRepositoryImpl implements MongoRepository {
    @Autowired
    @Getter
    private MongoTemplate mongoTemplate;

    public <T> T save(T document) {

        return this.mongoTemplate.save(document);
    }
}
