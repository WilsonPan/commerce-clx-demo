package com.chatlabs.clxdemo.infrastructure.repository;

public interface MongoRepository {
    <T> T save(T document);
}
