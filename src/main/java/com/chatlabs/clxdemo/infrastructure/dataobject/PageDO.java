package com.chatlabs.clxdemo.infrastructure.dataobject;

import java.util.Date;

import com.alibaba.fastjson.JSONObject;
import com.chatlabs.clxdemo.domain.Page.PageStatus;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

import lombok.Data;

@Data
@Document("page")
public class PageDO {
    @MongoId
    private ObjectId id;
    private String title;
    private String remark;
    private JSONObject data;
    private PageStatus status;
    @Field(name = "tenant_id")
    private String tenantId;
    private String creator;
    @Field(name = "create_time")
    private Date createTime;
    @Field(name = "update_time")
    private Date updateTime;
}
