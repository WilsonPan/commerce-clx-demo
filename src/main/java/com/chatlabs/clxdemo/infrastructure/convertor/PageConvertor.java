package com.chatlabs.clxdemo.infrastructure.convertor;

import java.util.List;

import com.chatlabs.clxdemo.client.dto.CreatePageDTO;
import com.chatlabs.clxdemo.client.dto.ListPageDTO;
import com.chatlabs.clxdemo.client.dto.UpdatePageDTO;
import com.chatlabs.clxdemo.domain.Page.Page;
import com.chatlabs.clxdemo.infrastructure.dataobject.PageDO;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

@Component
@Mapper(uses = EnumConverter.class)
public interface PageConvertor {
    PageConvertor INSTANCE = Mappers.getMapper(PageConvertor.class);

    @Mapping(target = "createTime", expression = "java(new java.util.Date())")
    Page toPageEntity(CreatePageDTO createPageDTO);

    @Mapping(target = "id", expression = "java(new org.bson.types.ObjectId(updatePageDTO.getId()))")
    @Mapping(target = "updateTime", expression = "java(new java.util.Date())")
    Page toPageEntity(UpdatePageDTO updatePageDTO);

    PageDO toPageDO(Page page);

    @Mapping(target = "id", expression = "java(pageDO.getId().toString())")
    @Mapping(target = "createTime", source = "createTime", dateFormat = "yyyy-MM-dd HH:mm:ss")
    ListPageDTO toListPageDTO(PageDO pageDO);

    List<ListPageDTO> toListPageDTO(List<PageDO> pList);
}
