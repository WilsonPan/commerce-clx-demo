package com.chatlabs.clxdemo.adapter;

import java.util.Date;
import java.util.List;

import com.chatlabs.clxdemo.client.api.PageService;
import com.chatlabs.clxdemo.client.dto.CreatePageDTO;
import com.chatlabs.clxdemo.client.dto.ListPageDTO;
import com.chatlabs.clxdemo.client.dto.UpdatePageDTO;
import com.chatlabs.clxdemo.domain.Page.Page;
import com.chatlabs.clxdemo.domain.Page.PageStatus;
import com.chatlabs.clxdemo.infrastructure.convertor.PageConvertor;
import com.chatlabs.clxdemo.infrastructure.dataobject.PageDO;
import com.chatlabs.clxdemo.infrastructure.repository.PageRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/page")
public class PageController {
    @Autowired
    private PageService pageService;

    @Autowired
    private PageRepository pageRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @GetMapping("/get/{id}")
    public PageDO get(@PathVariable("id") String id) {
        Criteria criteria = Criteria.where("_id").is(new ObjectId(id));

        Query query = new Query(criteria);

        return mongoTemplate.findOne(query, PageDO.class);

    }

    @GetMapping("/list")
    public List<ListPageDTO> list() {

        Criteria criteria = new Criteria();

        Query query = new Query(criteria).skip(0).limit(2);

        List<PageDO> list = mongoTemplate.find(query, PageDO.class);

        return PageConvertor.INSTANCE.toListPageDTO(list);
    }

    @PostMapping("/save")
    public PageDO save(@RequestBody CreatePageDTO pageDTO) {
        PageDO pageDO = new PageDO();
        pageDO.setTitle(pageDTO.getTitle());
        pageDO.setCreateTime(new Date());
        pageDO.setCreator("Wilson");
        pageDO.setStatus(PageStatus.INIT);
        pageDO.setRemark("W");

        return this.mongoTemplate.save(pageDO);
    }

    @PutMapping("/update")
    public String update(@RequestBody UpdatePageDTO updatePageDTO) {

        Page page = PageConvertor.INSTANCE.toPageEntity(updatePageDTO);

        PageDO pageDO = PageConvertor.INSTANCE.toPageDO(page);

        Criteria criteria = new Criteria();
        Query query = new Query(criteria);
        Update update = Update.update("title", updatePageDTO.getTitle());
        update.set("key", "");
        mongoTemplate.updateFirst(query, update, PageDO.class);

        return "ok";
    }

}
