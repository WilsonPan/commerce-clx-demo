package com.chatlabs.clxdemo.adapter.viewobject;

import lombok.Data;

@Data
public class BaseVO {
    private String id;
}
