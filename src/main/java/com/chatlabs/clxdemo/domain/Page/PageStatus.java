package com.chatlabs.clxdemo.domain.Page;

import lombok.Getter;

public enum PageStatus {
    INIT(0, "初始化");

    @Getter
    private int value;
    @Getter
    private String desc;

    private PageStatus(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static PageStatus getStatus(int value) {
        switch (value) {
            case 0:
                return INIT;
        }

        return null;
    }

}
