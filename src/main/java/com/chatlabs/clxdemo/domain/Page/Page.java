package com.chatlabs.clxdemo.domain.Page;

import java.util.Date;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;

import org.bson.types.ObjectId;

import lombok.Data;

@Data
public class Page {
    private ObjectId id;
    private String title;
    private String remark;
    private JSONObject data;
    private PageStatus status;
    @JSONField(name = "create_time")
    private Date createTime;
    @JSONField(name = "update_time")
    private Date updateTime;

    public int getStatusValue() {
        if (status == null) {
            return 0;
        }

        return this.getStatus().getValue();
    }

}