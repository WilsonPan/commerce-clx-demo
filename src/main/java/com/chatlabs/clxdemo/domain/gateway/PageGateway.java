package com.chatlabs.clxdemo.domain.gateway;

import com.chatlabs.clxdemo.domain.Page.Page;

import org.bson.types.ObjectId;

public interface PageGateway {
    ObjectId savePage(Page page);
}
