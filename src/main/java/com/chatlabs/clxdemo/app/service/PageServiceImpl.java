package com.chatlabs.clxdemo.app.service;

import java.util.List;

import com.chatlabs.clxdemo.client.api.PageService;
import com.chatlabs.clxdemo.client.dto.CreatePageDTO;
import com.chatlabs.clxdemo.client.dto.ListPageDTO;
import com.chatlabs.clxdemo.domain.Page.Page;
import com.chatlabs.clxdemo.domain.gateway.PageGateway;
import com.chatlabs.clxdemo.infrastructure.convertor.PageConvertor;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PageServiceImpl implements PageService {

    @Autowired
    private PageGateway pageGateway;

    @Override
    public ObjectId save(CreatePageDTO createPageDTO) {

        Page page = PageConvertor.INSTANCE.toPageEntity(createPageDTO);
        return this.pageGateway.savePage(page);
    }

    @Override
    public List<ListPageDTO> list() {
        // TODO Impl
        return null;
    }
}
