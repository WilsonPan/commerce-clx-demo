@host=http://localhost:9000/page

### get 
GET {{host}}/get/62207f9f84f9d01176c72448 HTTP/1.1

### get list

GET {{host}}/list HTTP/1.1


### create page

POST {{host}}/save HTTP/1.1
Content-Type: application/json

{
    "title": "Test",
    "remark": "Wilson Test",
    "data": {
        "color": "red"
    }       
}

### update page

PUT {{host}}/update HTTP/1.1
Content-Type: application/json

{
   "id": "621f02dae4bcb570372cc8a8",
    "title": "Test {{$datetime 'YYYY-MM-DD'}}",
    "remark": "Wilson Test",
    "data": {
        "color": "blue"
    }       
}


